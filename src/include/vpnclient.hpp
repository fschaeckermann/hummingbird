/*
 * vpnclient.hpp
 *
 * This file is part of AirVPN's Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef VPNCLIENT_HPP
#define VPNCLIENT_HPP

#include <stdlib.h>
#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <thread>
#include <memory>
#include <mutex>
#include <vector>
#include <map>
#include <dirent.h>
#include <errno.h>
#include <resolv.h>
#include <arpa/inet.h>
#include <openvpn/common/platform.hpp>
#include <openvpn/common/split.hpp>

#define USE_TUN_BUILDER

#if defined(OPENVPN_PLATFORM_LINUX)

    #include "dnsmanager.hpp"

#endif

#ifdef OPENVPN_PLATFORM_MAC

    #include <CoreFoundation/CFBundle.h>
    #include <ApplicationServices/ApplicationServices.h>
    #include <SystemConfiguration/SystemConfiguration.h>

#endif

// don't export core symbols
#define OPENVPN_CORE_API_VISIBILITY_HIDDEN

#define OPENVPN_DEBUG_VERBOSE_ERRORS
// #define OPENVPN_DEBUG_CLIPROTO

#include "execproc.h"
#include "localnetwork.hpp"
#include "netfilter.hpp"

// If enabled, don't direct ovpn3 core logging to
// ClientAPI::OpenVPNClient::log() virtual method.
// Instead, logging will go to LogBaseSimple::log().
// In this case, make sure to define:
//   LogBaseSimple log;
// at the top of your main() function to receive
// log messages from all threads.
// Also, note that the OPENVPN_LOG_GLOBAL setting
// MUST be consistent across all compilation units.

#ifdef OPENVPN_USE_LOG_BASE_SIMPLE

    #define OPENVPN_LOG_GLOBAL // use global rather than thread-local  object pointer

    #include <openvpn/log/logbasesimple.hpp>

#endif

#include <client/ovpncli.cpp>

#if defined(OPENVPN_PLATFORM_LINUX)

    // use SITNL by default
    #ifndef OPENVPN_USE_IPROUTE2

        #define OPENVPN_USE_SITNL

    #endif

    #include <openvpn/tun/linux/client/tuncli.hpp>

    // we use a static polymorphism and define a
    // platform-specific TunSetup class, responsible
    // for setting up tun device

    #define TUN_CLASS_SETUP TunLinuxSetup::Setup<TUN_LINUX>

#elif defined(OPENVPN_PLATFORM_MAC)

    #include <openvpn/tun/mac/client/tuncli.hpp>

    #define TUN_CLASS_SETUP TunMac::Setup

#endif

// should be included before other openvpn includes,
// with the exception of openvpn/log includes

#include <openvpn/common/exception.hpp>
#include <openvpn/common/string.hpp>
#include <openvpn/common/signal.hpp>
#include <openvpn/common/file.hpp>
#include <openvpn/common/getopt.hpp>
#include <openvpn/common/getpw.hpp>
#include <openvpn/common/cleanup.hpp>
#include <openvpn/time/timestr.hpp>
#include <openvpn/ssl/peerinfo.hpp>
#include <openvpn/ssl/sslchoose.hpp>

#ifdef OPENVPN_REMOTE_OVERRIDE

    #include <openvpn/common/process.hpp>

#endif

#if defined(USE_MBEDTLS)

    #include <openvpn/mbedtls/util/pkcs1.hpp>

#endif

using namespace openvpn;

enum IPClass
{
    v4,
    v6,
    Unknown
};

class VpnClientBase : public ClientAPI::OpenVPNClient, public LocalNetwork
{
    protected:

    NetFilter *netFilter = nullptr;

#if defined(OPENVPN_PLATFORM_LINUX)
    DNSManager *dnsManager = nullptr;
#endif

    std::vector<IPEntry> dnsTable;
    std::vector<IPEntry> systemDnsTable;
    std::vector<IPEntry> remoteServerIpList;
    TUN_CLASS_SETUP::Config tunnelConfig;
    ClientAPI::Config config;
    ClientAPI::EvalConfig evalConfig;
    bool dnsHasBeenPushed = false;
    NetFilter::Mode networkLockMode = NetFilter::Mode::UNKNOWN;
    bool dnsPushIgnored = false;
    bool netFilterIsPrivate = true;

    std::string resourceDirectory;
    std::string dnsBackupFile;
    std::string resolvDotConfFile;

    public:

    VpnClientBase(NetFilter::Mode netFilterMode, const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile);
    VpnClientBase(NetFilter *externalNetFilter, const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile);
    ~VpnClientBase();

    void setup(const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile);

    bool tun_builder_new() override;
    int tun_builder_establish() override;
    bool tun_builder_add_address(const std::string &address, int prefix_length, const std::string &gateway, bool ipv6, bool net30) override;
    bool tun_builder_add_route(const std::string &address, int prefix_length, int metric, bool ipv6) override;
    bool tun_builder_reroute_gw(bool ipv4, bool ipv6, unsigned int flags) override;
    bool tun_builder_set_remote_address(const std::string &address, bool ipv6) override;
    bool tun_builder_set_session_name(const std::string &name) override;
    bool tun_builder_exclude_route(const std::string& address, int prefix_length, bool ipv6);
    bool tun_builder_add_dns_server(const std::string &address, bool ipv6) override;
    void tun_builder_teardown(bool disconnect) override;
    bool socket_protect(int socket, std::string remote, bool ipv6) override;
    bool ignore_dns_push() override;

    void setConfig(ClientAPI::Config c);
    ClientAPI::Config getConfig();
    void setEvalConfig(ClientAPI::EvalConfig e);
    ClientAPI::EvalConfig getEvalConfig();
    void setNetworkLockMode(NetFilter::Mode mode);
    NetFilter::Mode getNetworkLockMode();
    void ignoreDnsPush(bool ignore);
    bool isDnsPushIgnored();
    std::vector<IPEntry> getPushedDns();

    static std::string openVPNInfo();
    static std::string openVPNCopyright();
    static std::string sslLibraryVersion();

    private:

    TUN_CLASS_SETUP::Ptr tunnelSetup = new TUN_CLASS_SETUP();
    TunBuilderCapture tunnelBuilderCapture;
};

class VpnClient : public VpnClientBase
{
    public:

    enum ClockTickAction
    {
        CT_UNDEF,
        CT_STOP,
        CT_RECONNECT,
        CT_PAUSE,
        CT_RESUME,
        CT_STATS,
    };

    enum Status
    {
        DISCONNECTED,
        CONNECTED,
        CONNECTING,
        RECONNECTING,
        PAUSED,
        RESUMING,
        CLIENT_RESTART,
        CONNECTION_TIMEOUT,
        INACTIVE_TIMEOUT
    };

    enum RestoreNetworkMode
    {
        FULL,
        DNS_ONLY,
        FIREWALL_ONLY
    };

    struct EventData
    {
        ClientAPI::Event vpnEvent;
        ClientAPI::ConnectionInfo connectionInfo;
        Status status;
        std::string message;
    };

    typedef void (*EventCallbackFunction)(EventData data);
    typedef void EventCallback;

    VpnClient(NetFilter::Mode netFilterMode, const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile);
    VpnClient(NetFilter *externalNetFilter, const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile);
    ~VpnClient();

    Status getStatus();
    bool eventError();
    bool eventFatalError();

    bool is_dynamic_challenge() const;
    std::string dynamic_challenge_cookie();

    std::string epki_ca;
    std::string epki_cert;

#if defined(USE_MBEDTLS)

    MbedTLSPKI::PKContext epki_ctx; // external PKI context

#endif

    void set_clock_tick_action(const ClockTickAction action);
    void print_stats();
    std::map<std::string, std::string> get_connection_stats();

    bool restoreNetworkSettings(RestoreNetworkMode mode = RestoreNetworkMode::FULL, bool stdOutput = false);

#ifdef OPENVPN_REMOTE_OVERRIDE

    void set_remote_override_cmd(const std::string &cmd);

#endif
    
    void setConnectionInformation(const std::string &s);
    void setExcludeRoutes(std::vector<std::string>);

    bool subscribeEvent(ClientEvent::Type event, EventCallbackFunction callbackFunction);
    bool unsubscribeEvent(ClientEvent::Type event, EventCallbackFunction callbackFunction);

    static bool isDataCipherSupported(std::string cipher);
    static std::vector<std::string> getSupportedDataCiphers();
    
    private:

    virtual void event(const ClientAPI::Event &ev) override;
    virtual void log(const ClientAPI::LogInfo &log) override;
    virtual void clock_tick() override;
    virtual void external_pki_cert_request(ClientAPI::ExternalPKICertRequest &certreq) override;
    virtual void external_pki_sign_request(ClientAPI::ExternalPKISignRequest &signreq) override;

    static int rng_callback(void *arg, unsigned char *data, size_t len);

    virtual bool pause_on_connection_timeout() override;

#ifdef OPENVPN_REMOTE_OVERRIDE

    virtual bool remote_override_enabled() override;
    virtual void remote_override(ClientAPI::RemoteOverride &ro);

#endif

    void init();
    void addExcludeRoute(const std::string &serverName, IPClass ipclass, const std::string &serverIP, int prefix_len);
    void resolveAddressWithPrefixLen(std::string addr_and_prefix, void(VpnClient::*addResolvedAddress)(const std::string &, IPClass, const std::string &, int));

    void addServer(const std::string &serverName, IPClass ipclass, const std::string &serverIP, int prefix_len);

    void onDisconnectedEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onConnectedEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onReconnectingEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onAuthPendingEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onResolveEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onWaitEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onWaitProxyEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onConnectingEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onGetConfigEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onAssignIpEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onAddRoutesEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onEchoOptEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onInfoEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onWarnEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onPauseEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onResumeEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onRelayEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onCompressionEnabledEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onUnsupportedFeatureEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onTransportErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onTunErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onClientRestartEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onAuthFailedEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onCertVerifyFailEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onTlsVersionMinEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onClientHaltEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onClientSetupEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onTunHaltEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onConnectionTimeoutEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onInactiveTimeoutEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onDynamicChallengeEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onProxyNeedCredsEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onProxyErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onTunSetupFailedEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onTunIFaceCreateEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onTunIFaceDisabledEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onEpkiErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onEpkiInvalidAliasEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);
    void onRelayErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo);

    void raiseEvent(ClientEvent::Type event, const EventData &data);

    std::mutex log_mutex;
    std::string dc_cookie;
    std::string connectionInformation;

    std::map<ClientEvent::Type, EventCallbackFunction> eventSubscription;
    std::map<ClientEvent::Type, EventCallbackFunction>::iterator eventSubscriptionIterator;

    static std::vector<std::string> supportedDataCipher;

    RandomAPI::Ptr rng;      // random data source for epki
    volatile ClockTickAction clock_tick_action = CT_UNDEF;
    Status status;
    bool event_error;
    bool event_fatal_error;

    std::vector<std::string> exclude_routes = std::vector<std::string>();

#ifdef OPENVPN_REMOTE_OVERRIDE

    std::string remote_override_cmd;

#endif

};

using namespace openvpn;

// VpnClientBase class

VpnClientBase::VpnClientBase(NetFilter::Mode netFilterMode, const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile)
{
    networkLockMode = netFilterMode;

    if(networkLockMode != NetFilter::Mode::OFF && networkLockMode != NetFilter::Mode::UNKNOWN)
    {
        netFilter = new NetFilter(resDir, networkLockMode);

        netFilterIsPrivate = true;

        if(netFilter != nullptr && netFilter->init())
            OPENVPN_LOG("Network filter successfully initialized");
        else
        {
            OPENVPN_LOG("ERROR: Cannot initialize network filter");
        
            netFilterIsPrivate = false;

            networkLockMode = NetFilter::Mode::OFF;
        }
    }
    else
    {
        netFilter = nullptr;

        netFilterIsPrivate = false;
    }

    setup(resDir, dnsBkpFile, resolvConfFile);
}

VpnClientBase::VpnClientBase(NetFilter *externalNetFilter, const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile)
{
    netFilter = externalNetFilter;

    netFilterIsPrivate = false;

    if(netFilter != nullptr)
    {
        networkLockMode = netFilter->getMode();
    }
    else
    {
        netFilterIsPrivate = false;

        networkLockMode = NetFilter::Mode::OFF;

        OPENVPN_LOG("WARNING: provided a null NetFilter object. Network filter and lock are disabled.");
    }

    setup(resDir, dnsBkpFile, resolvConfFile);
}

VpnClientBase::~VpnClientBase()
{
    if(netFilter != nullptr && netFilterIsPrivate == true)
        delete netFilter;

#if defined(OPENVPN_PLATFORM_LINUX)

    if(dnsManager != nullptr)
        delete dnsManager;

#endif
}

void VpnClientBase::setup(const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile)
{
    resourceDirectory = resDir;
    dnsBackupFile = dnsBkpFile;
    resolvDotConfFile = resolvConfFile;

#if defined(OPENVPN_PLATFORM_LINUX)

    dnsManager = new DNSManager(resolvDotConfFile);

#endif

    dnsHasBeenPushed = false;
    dnsPushIgnored = false;
}

bool VpnClientBase::tun_builder_new()
{
    tunnelBuilderCapture.tun_builder_set_mtu(1500);

    return true;
}

int VpnClientBase::tun_builder_establish()
{
    if(!tunnelSetup)
        tunnelSetup.reset(new TUN_CLASS_SETUP());

    tunnelConfig.layer = Layer(Layer::Type::OSI_LAYER_3);

    // no need to add bypass routes on establish since we do it on socket_protect

    tunnelConfig.add_bypass_routes_on_establish = false;

    return tunnelSetup->establish(tunnelBuilderCapture, &tunnelConfig, nullptr, std::cout);
}

bool VpnClientBase::tun_builder_add_address(const std::string &address, int prefix_length, const std::string &gateway, bool ipv6, bool net30)
{
    std::ostringstream os;

    if(isIPv6Enabled() == false && ipv6 == true)
    {
        os.str("");

        os << "ERROR: Cannot add address " << address << " pushed by the server. IPv6 is not available in this system";
        
        OPENVPN_LOG(os.str());

        return false;
    }
    
    return tunnelBuilderCapture.tun_builder_add_address(address, prefix_length, gateway, ipv6, net30);
}

bool VpnClientBase::tun_builder_add_route(const std::string &address, int prefix_length, int metric, bool ipv6)
{
    std::ostringstream os;

    if(isIPv6Enabled() == false && ipv6 == true)
    {
        os.str("");

        os << "ERROR: Cannot add route to address " << address << " pushed by the server. IPv6 is not available in this system";
        
        OPENVPN_LOG(os.str());

        return false;
    }
    
    return tunnelBuilderCapture.tun_builder_add_route(address, prefix_length, metric, ipv6);
}

bool VpnClientBase::tun_builder_reroute_gw(bool ipv4, bool ipv6, unsigned int flags)
{
    std::ostringstream os;

    if(isIPv6Enabled() == false && ipv6 == true)
    {
        os.str("");

        os << "ERROR: Cannot reroute gateway to IPv6 addresses. IPv6 is not available in this system";
        
        OPENVPN_LOG(os.str());

        return false;
    }
    
    return tunnelBuilderCapture.tun_builder_reroute_gw(ipv4, ipv6, flags);
}

bool VpnClientBase::tun_builder_set_remote_address(const std::string &address, bool ipv6)
{
    std::ostringstream os;

    if(isIPv6Enabled() == false && ipv6 == true)
    {
        os.str("");

        os << "ERROR: Cannot set remote address " << address << " pushed by the server is ignored. IPv6 is not available in this system";
        
        OPENVPN_LOG(os.str());

        return false;
    }
    
    return tunnelBuilderCapture.tun_builder_set_remote_address(address, ipv6);
}

bool VpnClientBase::tun_builder_set_session_name(const std::string &name)
{
    return tunnelBuilderCapture.tun_builder_set_session_name(name);
}

bool VpnClientBase::tun_builder_exclude_route(const std::string& address, int prefix_length, bool ipv6)
{
    return tunnelBuilderCapture.tun_builder_exclude_route(address, prefix_length, 0, ipv6);
}

bool VpnClientBase::tun_builder_add_dns_server(const std::string &address, bool ipv6)
{
    std::ostringstream os;
    IPEntry dnsEntry;
    NetFilter::IP filterIP;
    std::string dnsFilterIP;

    if(isIPv6Enabled() == false && ipv6 == true)
    {
        os.str("");

        os << "ERROR: Cannot add DNS " << address << " pushed by the server. IPv6 is not available in this system";
        
        OPENVPN_LOG(os.str());

        return false;
    }
    
    if(dnsPushIgnored == false)
    {
        os.str("");

        os << "VPN Server has pushed ";

        if(ipv6)
            os << "IPv6";
        else
            os << "IPv4";

        os << " DNS server " << address;

        OPENVPN_LOG(os.str());

        dnsEntry.address = address;
        dnsEntry.ipv6 = ipv6;

        dnsTable.push_back(dnsEntry);

        dnsHasBeenPushed = true;

        dnsFilterIP = address;

        if(ipv6)
        {
            filterIP = NetFilter::IP::v6;

            dnsFilterIP += "/128";
        }
        else
        {
            filterIP = NetFilter::IP::v4;

            dnsFilterIP += "/32";
        }

        if(netFilter != nullptr && netFilter->isNetworkLockAvailable())
            netFilter->commitAllowRule(filterIP, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, dnsFilterIP, 0);
    }

#if defined(OPENVPN_PLATFORM_LINUX)

    DNSManager::Error retval;
    bool setDefault;

    if(dnsPushIgnored == false)
    {
        os.str("");

        retval = dnsManager->addAddressToResolvDotConf(address, ipv6);

        if(retval == DNSManager::Error::OK)
        {
            os << "Setting pushed ";

            if(ipv6)
                os << "IPv6";
            else
                os << "IPv4";

            os << " DNS server " << address << " in resolv.conf";
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_OPEN_ERROR)
        {
            os << "ERROR: Cannot open resolv.conf";
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_READ_ERROR)
        {
            os << "ERROR: Cannot read resolv.conf";
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_RENAME_ERROR)
        {
            os << "ERROR: Cannot create a backup copy of resolv.conf";
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_WRITE_ERROR)
        {
            os << "ERROR: Cannot write in resolv.conf";
        }
        else
        {
            os << "ERROR: resolv.conf generic error";
        }

        OPENVPN_LOG(os.str());

        if(dnsManager->systemHasSystemdResolved() && dnsManager->systemHasResolvectl())
        {
            scanInterfaces();

            for(std::string interface : localInterface)
            {
                os.str("");

                if(interface.substr(0, 3) == "tun")
                    setDefault = true;
                else
                    setDefault = false;
                    
                retval = dnsManager->addAddressToResolved(interface, address.c_str(), ipv6, setDefault);

                if(retval == DNSManager::Error::OK)
                {
                    os << "Setting pushed ";

                    if(ipv6)
                        os << "IPv6";
                    else
                        os << "IPv4";

                    os << " DNS server " << address << " for interface " << interface << " via systemd-resolved";
                    
                    if(setDefault == true)
                        os << " (default route)";
                }
                else if(retval == DNSManager::Error::RESOLVED_IS_NOT_AVAILABLE)
                {
                    os << "ERROR systemd-resolved is not available on this system";
                }
                else if(retval == DNSManager::Error::RESOLVED_ADD_DNS_ERROR)
                {
                    os << "ERROR systemd-resolved: Failed to add DNS server " << address << " for interface " << interface;
                }
                else if(retval == DNSManager::Error::RESOLVED_SET_DEFAULT_ROUTE_ERROR)
                {
                    os << "ERROR systemd-resolved: Failed to set default route for interface " << interface;
                }
                else if(retval == DNSManager::Error::NO_RESOLVED_COMMAND)
                {
                    os << "ERROR systemd-resolved: resolvectl or systemd-resolve command not found";
                }
                else
                {
                    os << "ERROR systemd-resolved: Unknown error while adding DNS server " << address << " for interface " << interface;
                }

                OPENVPN_LOG(os.str());
            }
        }
    }
    else
    {
        os.str("");

        os << "WARNING: ignoring server DNS push request for address " << address;

        OPENVPN_LOG(os.str());
    }

#endif

    return tunnelBuilderCapture.tun_builder_add_dns_server(address, ipv6);
}

void VpnClientBase::tun_builder_teardown(bool disconnect)
{
    std::ostringstream os;

    auto os_print = Cleanup([&os](){ OPENVPN_LOG_STRING(os.str()); });

    tunnelSetup->destroy(os);
}

bool VpnClientBase::socket_protect(int socket, std::string remote, bool ipv6)
{
    (void)socket;
    std::ostringstream os;

    if(isIPv6Enabled() == false && ipv6 == true)
    {
        os.str("");

        os << "ERROR: Cannot do socket protection for " << remote << " and pushed by the server. IPv6 is not available in this system";
        
        OPENVPN_LOG(os.str());

        return false;
    }

    auto os_print = Cleanup([&os](){ OPENVPN_LOG_STRING(os.str()); });

    return tunnelSetup->add_bypass_route(remote, ipv6, os);
}

bool VpnClientBase::ignore_dns_push()
{
    return dnsPushIgnored;
}

void VpnClientBase::setConfig(const ClientAPI::Config c)
{
    config = c;
}

ClientAPI::Config VpnClientBase::getConfig()
{
    return config;
}

void VpnClientBase::setEvalConfig(const ClientAPI::EvalConfig e)
{
    evalConfig = e;
}

ClientAPI::EvalConfig VpnClientBase::getEvalConfig()
{
    return evalConfig;
}

void VpnClientBase::setNetworkLockMode(NetFilter::Mode mode)
{
    if(netFilter != nullptr)
    {
        networkLockMode = mode;

        netFilter->setMode(mode);
    }
    else
    {
        networkLockMode = NetFilter::Mode::OFF;

        OPENVPN_LOG("WARNING: Network filter and lock are disabled. Mode setting is ignored");
    }
}

NetFilter::Mode VpnClientBase::getNetworkLockMode()
{
    return networkLockMode;
}

void VpnClientBase::ignoreDnsPush(bool ignore)
{
    dnsPushIgnored = ignore;
}

bool VpnClientBase::isDnsPushIgnored()
{
    return dnsPushIgnored;
}

std::vector<VpnClientBase::IPEntry> VpnClientBase::getPushedDns()
{
    std::vector<IPEntry> pushedDns(dnsTable);
    
    return pushedDns;
}

std::string VpnClientBase::openVPNInfo()
{
    return ClientAPI::OpenVPNClientHelper::platform();
}

std::string VpnClientBase::openVPNCopyright()
{
    return ClientAPI::OpenVPNClientHelper::copyright();
}

std::string VpnClientBase::sslLibraryVersion()
{
    return ClientAPI::OpenVPNClientHelper::ssl_library_version();
}

// VpnClient class

VpnClient::VpnClient(NetFilter::Mode netFilterMode, const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile) : VpnClientBase(netFilterMode, resDir, dnsBkpFile, resolvConfFile)
{
    init();
}

VpnClient::VpnClient(NetFilter *externalNetFilter, const std::string &resDir, const std::string &dnsBkpFile, const std::string &resolvConfFile) : VpnClientBase(externalNetFilter, resDir, dnsBkpFile, resolvConfFile)
{
    init();
}


VpnClient::~VpnClient()
{
}

void VpnClient::init()
{
    status = Status::DISCONNECTED;

    connectionInformation = "";
    
    event_error = false;
    event_fatal_error = false;
}

VpnClient::Status VpnClient::getStatus()
{
    return status;
}

bool VpnClient::eventError()
{
    return event_error;
}

bool VpnClient::eventFatalError()
{
    return event_fatal_error;
}

void VpnClient::setConnectionInformation(const std::string &s)
{
    connectionInformation = s;
}

void VpnClient::setExcludeRoutes(std::vector<std::string> routes)
{
    exclude_routes = routes;
}

bool VpnClient::is_dynamic_challenge() const
{
    return !dc_cookie.empty();
}

std::string VpnClient::dynamic_challenge_cookie()
{
    return dc_cookie;
}

void VpnClient::set_clock_tick_action(const ClockTickAction action)
{
    clock_tick_action = action;
}

void VpnClient::print_stats()
{
    const int n = stats_n();
    std::vector<long long> stats = stats_bundle();

    std::cout << "STATS:" << std::endl;

    for(int i = 0; i < n; ++i)
    {
        const long long value = stats[i];

        if(value)
            std::cout << "  " << stats_name(i) << " : " << value << std::endl;
    }
}

std::map<std::string, std::string> VpnClient::get_connection_stats()
{
    int n;
    std::vector<long long> stats;
    std::map<std::string, std::string> conn_stats;
    openvpn::ClientAPI::ConnectionInfo connectionInfo;

    connectionInfo = connection_info();

    conn_stats.insert(std::make_pair("user", connectionInfo.user));
    conn_stats.insert(std::make_pair("server_host", connectionInfo.serverHost));
    conn_stats.insert(std::make_pair("server_port", connectionInfo.serverPort));
    conn_stats.insert(std::make_pair("server_proto", connectionInfo.serverProto));
    conn_stats.insert(std::make_pair("server_ip", connectionInfo.serverIp));
    conn_stats.insert(std::make_pair("vpn_ipv4", connectionInfo.vpnIp4));
    conn_stats.insert(std::make_pair("vpn_ipv6", connectionInfo.vpnIp6));
    conn_stats.insert(std::make_pair("gateway_ipv4", connectionInfo.gw4));
    conn_stats.insert(std::make_pair("gateway_ipv6", connectionInfo.gw6));
    conn_stats.insert(std::make_pair("client_ip", connectionInfo.clientIp));
    conn_stats.insert(std::make_pair("tun_name", connectionInfo.tunName));
    conn_stats.insert(std::make_pair("topology", connectionInfo.topology));
    conn_stats.insert(std::make_pair("cipher", connectionInfo.cipher));
    conn_stats.insert(std::make_pair("ping", std::to_string(connectionInfo.ping)));
    conn_stats.insert(std::make_pair("ping_restart", std::to_string(connectionInfo.ping_restart)));

    n = stats_n();
    stats = stats_bundle();

    for(int i = 0; i < n; ++i)
    {
        const long long value = stats[i];

        if(value)
            conn_stats.insert(std::make_pair(AirVPNTools::toLower(stats_name(i)), std::to_string(value)));
    }

    return conn_stats;
}

#ifdef OPENVPN_REMOTE_OVERRIDE

    void VpnClient::set_remote_override_cmd(const std::string &cmd)
    {
        remote_override_cmd = cmd;
    }

#endif

void VpnClient::event(const ClientAPI::Event &ev)
{
    std::ostringstream os;
    ClientAPI::ConnectionInfo connectionInfo = connection_info();

    os.str("");

    os << "EVENT: " << ev.name;

    if(ev.fatal)
    {
        os << " [FATAL ERROR]";
        
        event_fatal_error = true;
    }
    else if(ev.error)
    {
        os << " [ERROR]";
        
        event_error = true;
    }

    if(!ev.info.empty())
        os << " " << ev.info;

    OPENVPN_LOG(os.str());

    if(ev.name == "DISCONNECTED")
    {
        onDisconnectedEvent(ev, connectionInfo);
    }
    else if(ev.name == "CONNECTED")
    {
        onConnectedEvent(ev, connectionInfo);
    }
    else if(ev.name == "RECONNECTING")
    {
        onReconnectingEvent(ev, connectionInfo);
    }
    else if(ev.name == "AUTH_PENDING")
    {
        onAuthPendingEvent(ev, connectionInfo);
    }
    else if(ev.name == "RESOLVE")
    {
        onResolveEvent(ev, connectionInfo);
    }
    else if(ev.name == "WAIT")
    {
        onWaitEvent(ev, connectionInfo);
    }
    else if(ev.name == "WAIT_PROXY")
    {
        onWaitProxyEvent(ev, connectionInfo);
    }
    else if(ev.name == "CONNECTING")
    {
        onConnectingEvent(ev, connectionInfo);
    }
    else if(ev.name == "GET_CONFIG")
    {
        onGetConfigEvent(ev, connectionInfo);
    }
    else if(ev.name == "ASSIGN_IP")
    {
        onAssignIpEvent(ev, connectionInfo);
    }
    else if(ev.name == "ADD_ROUTES")
    {
        onAddRoutesEvent(ev, connectionInfo);
    }
    else if(ev.name == "ECHO")
    {
        onEchoOptEvent(ev, connectionInfo);
    }
    else if(ev.name == "INFO")
    {
        onInfoEvent(ev, connectionInfo);
    }
    else if(ev.name == "WARN")
    {
        onWarnEvent(ev, connectionInfo);
    }
    else if(ev.name == "PAUSE")
    {
        onPauseEvent(ev, connectionInfo);
    }
    else if(ev.name == "RESUME")
    {
        onResumeEvent(ev, connectionInfo);
    }
    else if(ev.name == "RELAY")
    {
        onRelayEvent(ev, connectionInfo);
    }
    else if(ev.name == "COMPRESSION_ENABLED")
    {
        onCompressionEnabledEvent(ev, connectionInfo);
    }
    else if(ev.name == "UNSUPPORTED_FEATURE")
    {
        onUnsupportedFeatureEvent(ev, connectionInfo);
    }
    else if(ev.name == "TRANSPORT_ERROR")
    {
        onTransportErrorEvent(ev, connectionInfo);
    }
    else if(ev.name == "TUN_ERROR")
    {
        onTunErrorEvent(ev, connectionInfo);
    }
    else if(ev.name == "CLIENT_RESTART")
    {
        onClientRestartEvent(ev, connectionInfo);
    }
    else if(ev.name == "AUTH_FAILED")
    {
        onAuthFailedEvent(ev, connectionInfo);
    }
    else if(ev.name == "CERT_VERIFY_FAIL")
    {
        onCertVerifyFailEvent(ev, connectionInfo);
    }
    else if(ev.name == "TLS_VERSION_MIN")
    {
        onTlsVersionMinEvent(ev, connectionInfo);
    }
    else if(ev.name == "CLIENT_HALT")
    {
        onClientHaltEvent(ev, connectionInfo);
    }
    else if(ev.name == "CLIENT_SETUP")
    {
        onClientSetupEvent(ev, connectionInfo);
    }
    else if(ev.name == "TUN_HALT")
    {
        onTunHaltEvent(ev, connectionInfo);
    }
    else if(ev.name == "CONNECTION_TIMEOUT")
    {
        onConnectionTimeoutEvent(ev, connectionInfo);
    }
    else if(ev.name == "INACTIVE_TIMEOUT")
    {
        onInactiveTimeoutEvent(ev, connectionInfo);
    }
    else if(ev.name == "DYNAMIC_CHALLENGE")
    {
        onDynamicChallengeEvent(ev, connectionInfo);
    }
    else if(ev.name == "PROXY_NEED_CREDS")
    {
        onProxyNeedCredsEvent(ev, connectionInfo);
    }
    else if(ev.name == "PROXY_ERROR")
    {
        onProxyErrorEvent(ev, connectionInfo);
    }
    else if(ev.name == "TUN_SETUP_FAILED")
    {
        onTunSetupFailedEvent(ev, connectionInfo);
    }
    else if(ev.name == "TUN_IFACE_CREATE")
    {
        onTunIFaceCreateEvent(ev, connectionInfo);
    }
    else if(ev.name == "TUN_IFACE_DISABLED")
    {
        onTunIFaceDisabledEvent(ev, connectionInfo);
    }
    else if(ev.name == "EPKI_ERROR")
    {
        onEpkiErrorEvent(ev, connectionInfo);
    }
    else if(ev.name == "EPKI_INVALID_ALIAS")
    {
        onEpkiInvalidAliasEvent(ev, connectionInfo);
    }
    else if(ev.name == "RELAY_ERROR")
    {
        onRelayErrorEvent(ev, connectionInfo);
    }
}

void VpnClient::addServer(const std::string &serverName, IPClass ipclass, const std::string &serverIP, int prefix_len)
{
    std::string filterServerIP;
    std::string protocol;
    IPEntry ipEntry;
    NetFilter::IP filterIPLevel;

    if(ipclass == IPClass::Unknown || serverIP == "" || serverIP == "0.0.0.0" || serverIP == "::")
        return;

    ipEntry.address = serverIP;

    filterServerIP = serverIP + "/" +std::to_string(prefix_len);

    if(ipclass == IPClass::v4)
    {
        filterIPLevel = NetFilter::IP::v4;
        protocol = "4";
        ipEntry.ipv6 = false;
        if (prefix_len<0) prefix_len = 32;
    }
    else
    {
        filterIPLevel = NetFilter::IP::v6;
        protocol = "6";
        ipEntry.ipv6 = true;
        if (prefix_len<0) prefix_len = 128;
    }

    if(std::find(remoteServerIpList.begin(), remoteServerIpList.end(), ipEntry) == remoteServerIpList.end())
    {
        std::ostringstream os;
        os.str("");

        if (serverIP.length() <= serverName.length() && serverName.substr(0, serverIP.length()) != serverIP)
        {
            os << "Resolved server " << serverName << " into IPv" << protocol << " " << serverIP;
            OPENVPN_LOG(os.str());;
        }

        if(netFilter != nullptr && netFilter->isNetworkLockAvailable())
        {
            os.str("");
            os << "Adding IPv" << protocol << " server " << serverIP << " to network filter";
            OPENVPN_LOG(os.str());
            netFilter->addAllowRule(filterIPLevel, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, filterServerIP, 0);
        }

        remoteServerIpList.push_back(ipEntry);

    }
}

void VpnClient::addExcludeRoute(const std::string &serverName, IPClass ipclass, const std::string &serverIP, int prefix_len)
{
    std::string filterServerIP;
    std::string protocol;
    NetFilter::IP filterIPLevel;
    std::ostringstream os;

    if(ipclass == IPClass::Unknown || serverIP == "" || serverIP == "0.0.0.0" || serverIP == "::")
        return;

    filterServerIP = serverIP + "/" +std::to_string(prefix_len);

    if(ipclass == IPClass::v4)
    {
        filterIPLevel = NetFilter::IP::v4;
        protocol = "4";
    }
    else
    {
        filterIPLevel = NetFilter::IP::v6;
        protocol = "6";
    }

    if (serverIP.length() > serverName.length() || serverName.substr(0, serverIP.length()) != serverIP)
    {
        os.str("");
        os << "Resolved server " << serverName << " into IPv" << protocol<< " " << serverIP;
        OPENVPN_LOG(os.str());;
    }

    if (VpnClientBase::tun_builder_exclude_route(serverIP, prefix_len, IPClass::v6==ipclass))
    {
        os.str("");
        os << "Registered exclude route for IPv" << protocol << " address " << serverIP << "/" << prefix_len;
        OPENVPN_LOG(os.str());;
    }

    if(netFilter != nullptr && netFilter->isNetworkLockAvailable())
    {
        os.str("");
        os << "Adding IPv" << protocol << " server " << serverIP << " to network filter";
        OPENVPN_LOG(os.str());
        netFilter->addAllowRule(filterIPLevel, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, filterServerIP, 0);
    }
}

void VpnClient::resolveAddressWithPrefixLen(std::string addr_and_prefix, void(VpnClient::*addResolvedAddress)(const std::string &, IPClass , const std::string &, int))
{
    std::ostringstream os;
    struct addrinfo ipinfo, *ipres = NULL, *current_ai;
    int aires;
    char resip[64];
    IPClass ipclass;
    int prefix_len;
    std::string serverName;

    OPENVPN_EXCEPTION(route_error);

    std::vector<std::string> pair;
    pair.reserve(2);
    Split::by_char_void<std::vector<std::string>, NullLex, Split::NullLimit>(pair, addr_and_prefix, '/', 0, 1);

    serverName = pair[0];
    if (pair.size() >= 2)
        prefix_len = parse_number_throw<unsigned int>(pair[1], "prefix length");
    else
        prefix_len = -1;

    memset(&ipinfo, 0, sizeof(ipinfo));

    ipinfo.ai_family = PF_UNSPEC;
    ipinfo.ai_flags = AI_NUMERICHOST;
    ipclass = IPClass::Unknown;

    aires = getaddrinfo(serverName.c_str(), NULL, &ipinfo, &ipres);

    if(aires || ipres == NULL)
    {
        ipclass = IPClass::Unknown;
    }
    else
    {
        switch(ipres->ai_family)
        {
            case AF_INET:
                ipclass = IPClass::v4;
                if (prefix_len>32) OPENVPN_THROW(route_error, "route: bad prefix len for IPv4 address: " << serverName);
                if (prefix_len<0) prefix_len = 32;
                break;
            case AF_INET6:
                ipclass = IPClass::v6;
                if (prefix_len>128) OPENVPN_THROW(route_error, "route: bad prefix len for IPv6 address: " << serverName);
                if (prefix_len<0) prefix_len = 128;
                break;
            default:
                ipclass = IPClass::Unknown;
                break;
        }
    }

    if(ipres != NULL)
    {
        freeaddrinfo(ipres);
        ipres = NULL;
    }

    if(ipclass == IPClass::Unknown)
    {
        if (prefix_len >= 0)
          OPENVPN_THROW(route_error, "route: prefix length not possible with DNS resolution: " << serverName);
        memset(&ipinfo, 0, sizeof(ipinfo));

        ipinfo.ai_family = PF_UNSPEC;

        aires = getaddrinfo(serverName.c_str(), NULL, &ipinfo, &ipres);

        if(aires == 0)
        {
            auto added_addresses_list = std::vector<std::string>();
            for(current_ai = ipres; current_ai != NULL; current_ai = current_ai->ai_next)
            {
                getnameinfo(current_ai->ai_addr, current_ai->ai_addrlen, resip, sizeof(resip), NULL, 0, NI_NUMERICHOST);

                std::string serverIP = resip;
                if(std::find(added_addresses_list.begin(), added_addresses_list.end(), serverIP) == added_addresses_list.end())
                {
                    added_addresses_list.push_back(serverIP);
                    int this_prefix_len = prefix_len;
                    switch(current_ai->ai_family)
                    {
                        case AF_INET:
                            ipclass = IPClass::v4;
                            if (this_prefix_len<0) this_prefix_len = 32;
                            break;
                        case AF_INET6:
                            ipclass = IPClass::v6;
                            if (this_prefix_len<0) this_prefix_len = 128;
                            break;
                        default:
                            ipclass = IPClass::Unknown;
                            break;
                    }
                    (this->*addResolvedAddress)(serverName, ipclass, serverIP, this_prefix_len);
                }
            }
        }
        else
        {
            ipclass = IPClass::Unknown;

            os.str("");
            os << "WARNING: Cannot resolve " << serverName << " (" << gai_strerror(aires) << ")";
            OPENVPN_LOG(os.str());
        }

        if(ipres != NULL)
        {
            freeaddrinfo(ipres);
            ipres = NULL;
        }
    }
    else
    {
        (this->*addResolvedAddress)(addr_and_prefix, ipclass, serverName, prefix_len);
    }
}


// event handlers

void VpnClient::onDisconnectedEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    restoreNetworkSettings();

    status = Status::DISCONNECTED;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::DISCONNECTED, eventData);
}

void VpnClient::onConnectedEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    std::ostringstream os;
    NetFilter::IP filterIP;
    EventData eventData;
    if(connectionInformation != "")
        OPENVPN_LOG(connectionInformation);

    if(netFilter != nullptr && netFilter->isNetworkLockAvailable())
        netFilter->addIgnoredInterface(tunnelConfig.iface_name);

#if defined(OPENVPN_PLATFORM_LINUX)

    DNSManager::Error retval;
    bool setDefault;

    if(dnsManager->systemHasSystemdResolved() && dnsManager->systemHasResolvectl())
    {
        scanInterfaces();

        for(std::string interface : localInterface)
        {
            if(interface.substr(0, 3) == "tun")
                setDefault = true;
            else
                setDefault = false;
                
            for(IPEntry dns : dnsTable)
            {
                os.str("");
                
                retval = dnsManager->addAddressToResolved(interface, dns.address.c_str(), dns.ipv6, setDefault);

                if(retval == DNSManager::Error::OK)
                {
                    os << "Setting pushed ";

                    if(dns.ipv6)
                        os << "IPv6";
                    else
                        os << "IPv4";

                    os << " DNS server " << dns.address << " for interface " << interface << " via systemd-resolved";
                    
                    if(setDefault == true)
                        os << " (default route)";
                }
                else if(retval == DNSManager::Error::RESOLVED_IS_NOT_AVAILABLE)
                {
                    os << "ERROR systemd-resolved is not available on this system";
                }
                else if(retval == DNSManager::Error::RESOLVED_ADD_DNS_ERROR)
                {
                    os << "ERROR systemd-resolved: Failed to add DNS server " << dns.address << " for interface " << interface;
                }
                else if(retval == DNSManager::Error::RESOLVED_SET_DEFAULT_ROUTE_ERROR)
                {
                    os << "ERROR systemd-resolved: Failed to set default route for interface " << interface;
                }
                else if(retval == DNSManager::Error::NO_RESOLVED_COMMAND)
                {
                    os << "ERROR systemd-resolved: resolvectl or systemd-resolve command not found";
                }
                else
                {
                    os << "ERROR systemd-resolved: Unknown error while adding DNS server " << dns.address << " for interface " << interface;
                }

                OPENVPN_LOG(os.str());
            }
        }
    }

#endif

    if(netFilter != nullptr && dnsHasBeenPushed == true && netFilter->isNetworkLockAvailable() && dnsPushIgnored == false)
    {
        os.str("");

        os << "Server has pushed its own DNS. Removing system DNS from network filter.";

        OPENVPN_LOG(os.str());

        for(IPEntry dns : systemDnsTable)
        {
            if(dns.ipv6 == true)
                filterIP = NetFilter::IP::v6;
            else
                filterIP = NetFilter::IP::v4;

            if(netFilter != nullptr)
                netFilter->commitRemoveAllowRule(filterIP, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, dns.address, 0);

            os.str("");

            os << "System DNS " << dns.address << " is now rejected by the network filter";

            OPENVPN_LOG(os.str());
        }
    }

    if(netFilter != nullptr && netFilter->isNetworkLockAvailable() && remoteServerIpList.size() > 1)
    {
        os.str("");

        os << "OpenVPN profile has multiple remote directives. Removing unused servers from network filter.";

        OPENVPN_LOG(os.str());

        for(IPEntry server : remoteServerIpList)
        {
            if(server.address != connectionInfo.serverIp)
            {
                if(server.ipv6 == true)
                    filterIP = NetFilter::IP::v6;
                else
                    filterIP = NetFilter::IP::v4;

                if(netFilter != nullptr)
                    netFilter->commitRemoveAllowRule(filterIP, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, server.address, 0);

                os.str("");

                os << "Server IPv";

                if(server.ipv6 == false)
                    os << "4";
                else
                    os << "6";

                os << " " << server.address << " has been removed from the network filter";

                OPENVPN_LOG(os.str());
            }
        }
    }

    status = Status::CONNECTED;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::CONNECTED, eventData);
}

void VpnClient::onReconnectingEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    if(config.tunPersist == false)
        restoreNetworkSettings(RestoreNetworkMode::DNS_ONLY);

    status = Status::RECONNECTING;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::RECONNECTING, eventData);
}

void VpnClient::onAuthPendingEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::AUTH_PENDING, eventData);
}

void VpnClient::onResolveEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    std::string serverIP, filterServerIP, protocol;
    std::ofstream systemDNSDumpFile;
    std::ostringstream os;
    NetFilter::IP filterIP;
    IPEntry ipEntry;
    EventData eventData;

#if defined(OPENVPN_PLATFORM_LINUX)

    if(status != Status::RECONNECTING)
    {
        if(dnsPushIgnored == false)
        {
            if(dnsManager->systemHasNetworkManager())
            {
                os.str("");

                os << "WARNING: NetworkManager is running on this system and may interfere with DNS management and cause DNS leaks";

                OPENVPN_LOG(os.str());
            }

            if(dnsManager->systemHasSystemdResolved())
            {
                os.str("");

                os << "WARNING: systemd-resolved is running on this system and may interfere with DNS management and cause DNS leaks";

                OPENVPN_LOG(os.str());
            }
        }
    }

#endif

    if(status != Status::RECONNECTING)
    {
        // save system DNS
        dnsTable.clear();
        systemDnsTable.clear();

        systemDNSDumpFile.open(dnsBackupFile);

#ifdef ON_ALPINE_LINUX
        res_init(); // initialize DNS name resolution in musl-libc
                    // but this does NOT provide name server addresses in _res.nsaddr_list!!!
                    // therefore we need to read the file /etc/resolv.conf directly ourselves.
        FILE *resolv_conf_file = fopen("/etc/resolv.conf", "r");
        if (resolv_conf_file)
        {
            char line[256], *addr;
            int i;
            while (fgets(line, sizeof(line), resolv_conf_file))
            {
                if (strlen(line)<12 || strncmp(line, "nameserver", 10) || !isspace(line[10]))
                    continue;
                ipEntry.ipv6 = false;
                for (addr = &line[11]; addr[0] && isspace(addr[0]); addr++);
                for (i = 0; addr[i] && !isspace(addr[i]); i++)
                    ipEntry.ipv6 |= (addr[i]==':');
                addr[i] = '\0';
                if (i<3 || ( i<7 && !ipEntry.ipv6 ))
                {
                    os.str("");
                    os << "Invalid nameserver specification ignored: " << line << std::endl;
                    OPENVPN_LOG(os.str());
                    continue;
                }
                ipEntry.address = addr;

                systemDnsTable.push_back(ipEntry);

                systemDNSDumpFile << ipEntry.address << std::endl;
            }
            fclose(resolv_conf_file);
        }
#else
        res_ninit(&_res);
        for(int i=0; i < MAXNS; i++)
        {
            if(_res.nsaddr_list[i].sin_addr.s_addr > 0)
            {
                ipEntry.address = inet_ntoa(_res.nsaddr_list[i].sin_addr);

                if(_res.nsaddr_list[i].sin_family == AF_INET)
                    ipEntry.ipv6 = false;
                else
                    ipEntry.ipv6 = true;

                systemDnsTable.push_back(ipEntry);

                systemDNSDumpFile << ipEntry.address << std::endl;
            }
        }
#endif
        systemDNSDumpFile.close();
    }

    if(netFilter != nullptr && netFilter->isNetworkLockAvailable())
    {
        if(status != Status::RECONNECTING)
        {
            for(IPEntry ip : localIPaddress)
            {
                os.str("");

                os << "Local IPv";

                if(ip.ipv6 == true)
                    os << "6";
                else
                    os << "4";

                os << " address " << ip.address;

                OPENVPN_LOG(os.str());
            }

            for(std::string interface : localInterface)
            {
                os.str("");

                os << "Local interface " << interface;

                OPENVPN_LOG(os.str());
            }

            if(netFilter != nullptr && netFilterIsPrivate == true)
                netFilter->setup(loopbackInterface);
        }

        os.str("");

        os << "Setting up network filter and lock";

        OPENVPN_LOG(os.str());

        // Allow system DNS to pass through network filter. It may be later denied by DNS push

        for(IPEntry dns : systemDnsTable)
        {
            if(dns.ipv6 == true)
                filterIP = NetFilter::IP::v6;
            else
                filterIP = NetFilter::IP::v4;

            if(netFilter != nullptr)
                netFilter->addAllowRule(filterIP, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, dns.address, 0);

            os.str("");

            os << "Allowing system DNS " << dns.address << " to pass through the network filter";

            OPENVPN_LOG(os.str());
        }

        if(netFilter != nullptr && netFilter->commitRules() == false)
        {
            os.str("");

            os << "ERROR: Cannot allow system DNS to pass through network filter";

            OPENVPN_LOG(os.str());
        }

        // Adding profile's remote entries to network filter

        if(evalConfig.remoteList.size() > 1)
        {
            os.str("");

            os << "OpenVPN profile has multiple remote directives. Temporarily adding remote servers to network filter.";

            OPENVPN_LOG(os.str());
        }

        remoteServerIpList.clear();
        for(ClientAPI::RemoteEntry remoteEntry : evalConfig.remoteList)
            resolveAddressWithPrefixLen(remoteEntry.server, &VpnClient::addServer);

        for(std::string route: exclude_routes)
            resolveAddressWithPrefixLen(route, &VpnClient::addExcludeRoute);

        os.str("");

        if(netFilter != nullptr && netFilter->commitRules() == true)
            os << "Network filter and lock successfully activated";
        else
            os << "ERROR: Cannot activate network filter and lock";

        OPENVPN_LOG(os.str());
    }

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::RESOLVE, eventData);
}

void VpnClient::onWaitEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::WAIT, eventData);
}

void VpnClient::onWaitProxyEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::WAIT_PROXY, eventData);
}

void VpnClient::onConnectingEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    status = Status::CONNECTING;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::CONNECTING, eventData);
}

void VpnClient::onGetConfigEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::GET_CONFIG, eventData);
}

void VpnClient::onAssignIpEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::ASSIGN_IP, eventData);
}

void VpnClient::onAddRoutesEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::ADD_ROUTES, eventData);
}

void VpnClient::onEchoOptEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::ECHO_OPT, eventData);
}

void VpnClient::onInfoEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::INFO, eventData);
}

void VpnClient::onWarnEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::WARN, eventData);
}

void VpnClient::onPauseEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    status = Status::PAUSED;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::PAUSE, eventData);
}

void VpnClient::onResumeEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    status = Status::RESUMING;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::RESUME, eventData);
}

void VpnClient::onRelayEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::RELAY, eventData);
}

void VpnClient::onCompressionEnabledEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::COMPRESSION_ENABLED, eventData);
}

void VpnClient::onUnsupportedFeatureEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::UNSUPPORTED_FEATURE, eventData);
}

void VpnClient::onTransportErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::TRANSPORT_ERROR, eventData);
}

void VpnClient::onTunErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::TUN_ERROR, eventData);
}

void VpnClient::onClientRestartEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    status = Status::CLIENT_RESTART;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::CLIENT_RESTART, eventData);
}

void VpnClient::onAuthFailedEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::AUTH_FAILED, eventData);
}

void VpnClient::onCertVerifyFailEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::CERT_VERIFY_FAIL, eventData);
}

void VpnClient::onTlsVersionMinEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::TLS_VERSION_MIN, eventData);
}

void VpnClient::onClientHaltEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::CLIENT_HALT, eventData);
}

void VpnClient::onClientSetupEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::CLIENT_SETUP, eventData);
}

void VpnClient::onTunHaltEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::TUN_HALT, eventData);
}

void VpnClient::onConnectionTimeoutEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    status = Status::CONNECTION_TIMEOUT;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::CONNECTION_TIMEOUT, eventData);
}

void VpnClient::onInactiveTimeoutEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    status = Status::INACTIVE_TIMEOUT;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::INACTIVE_TIMEOUT, eventData);
}

void VpnClient::onDynamicChallengeEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    std::ostringstream os;
    EventData eventData;

    dc_cookie = ev.info;

    ClientAPI::DynamicChallenge dc;

    if(ClientAPI::OpenVPNClientHelper::parse_dynamic_challenge(ev.info, dc))
    {
        os << "DYNAMIC CHALLENGE" << std::endl;
        os << "challenge: " << dc.challenge << std::endl;
        os << "echo: " << dc.echo << std::endl;
        os << "responseRequired: " << dc.responseRequired << std::endl;
        os << "stateID: " << dc.stateID << std::endl;

        OPENVPN_LOG(os.str());
    }

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::DYNAMIC_CHALLENGE, eventData);
}

void VpnClient::onProxyNeedCredsEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::PROXY_NEED_CREDS, eventData);
}

void VpnClient::onProxyErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::PROXY_ERROR, eventData);
}

void VpnClient::onTunSetupFailedEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::TUN_SETUP_FAILED, eventData);
}

void VpnClient::onTunIFaceCreateEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::TUN_IFACE_CREATE, eventData);
}

void VpnClient::onTunIFaceDisabledEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::TUN_IFACE_DISABLED, eventData);
}

void VpnClient::onEpkiErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::EPKI_ERROR, eventData);
}

void VpnClient::onEpkiInvalidAliasEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::EPKI_INVALID_ALIAS, eventData);
}

void VpnClient::onRelayErrorEvent(const ClientAPI::Event &ev, const ClientAPI::ConnectionInfo &connectionInfo)
{
    EventData eventData;

    eventData.status = status;
    eventData.vpnEvent = ev;
    eventData.connectionInfo = connectionInfo;
    eventData.message = "";

    raiseEvent(ClientEvent::Type::RELAY_ERROR, eventData);
}

bool VpnClient::restoreNetworkSettings(RestoreNetworkMode mode, bool stdOutput)
{
    std::ostringstream os;
    bool success = true;
    NetFilter *localNetFilter = nullptr;

#if defined(OPENVPN_PLATFORM_LINUX)

    if(dnsPushIgnored == false && (mode == RestoreNetworkMode::FULL || mode == RestoreNetworkMode::DNS_ONLY))
    {
        DNSManager::Error retval;

        // restore resolv.conf

        retval = dnsManager->restoreResolvDotConf();

        os.str("");

        if(retval == DNSManager::Error::OK)
        {
            os << "Successfully restored DNS settings";
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_OPEN_ERROR)
        {
            os << "WARNING: resolv.conf not found. DNS settings do not need to be restored.";

            success = true;
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_RESTORE_NOT_FOUND)
        {
            os << "WARNING: Backup copy of resolv.conf not found. DNS settings do not need to be restored.";

            success = true;
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_RESTORE_ERROR)
        {
            os << "ERROR: Cannot restore DNS settings.";

            success = false;
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_BKUP_READ_ERROR)
        {
            os << "ERROR: Cannot read resolv.conf backup file.";

            success = false;
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_BKUP_DELETE_ERROR)
        {
            os << "ERROR: Cannot delete resolv.conf backup file.";

            success = false;
        }
        else
        {
            os << "ERROR: Cannot restore DNS settings. Unknown error.";

            success = false;
        }

        os << std::endl;

        if(stdOutput == false)
            OPENVPN_LOG_STRING(os.str());
        else
            std::cout << os.str();

        if(dnsManager->systemHasSystemdResolved())
        {
            os.str("");

            retval = dnsManager->restoreResolved();

            if(retval == DNSManager::Error::OK)
            {
                os << "Restoring systemd-resolved DNS settings";
            }
            else if(retval == DNSManager::Error::RESOLVED_IS_NOT_AVAILABLE)
            {
                os << "ERROR systemd-resolved is not available on this system";

                success = false;
            }
            else if(retval == DNSManager::Error::RESOLVED_REVERT_DNS_ERROR)
            {
                os << "ERROR systemd-resolved: Failed to revert DNS configuration";

                success = false;
            }
            else if(retval == DNSManager::Error::RESOLVED_RESTORE_ERROR)
            {
                os << "ERROR systemd-resolved: Failed to restore DNS configuration";

                success = false;
            }
            else if(retval == DNSManager::Error::NO_RESOLVED_COMMAND)
            {
                os << "ERROR systemd-resolved: resolvectl or systemd-resolve command not found";

                success = false;
            }
            else
            {
                os << "ERROR systemd-resolved: Unknown error while reverting DNS";

                success = false;
            }

            os << std::endl;

            if(stdOutput == false)
                OPENVPN_LOG_STRING(os.str());
            else
                std::cout << os.str();
        }
    }

#endif

#if defined(OPENVPN_PLATFORM_MAC)

    if(dnsPushIgnored == false && (mode == RestoreNetworkMode::FULL || mode == RestoreNetworkMode::DNS_ONLY))
    {
        std::ifstream systemDNSDumpFile;
        std::string line;
        SCDynamicStoreRef dnsStore;
        CFMutableArrayRef dnsArray;
        CFDictionaryRef dnsDictionary;
        CFArrayRef dnsList;
        CFIndex ndx, listItems;
        bool setValueSuccess = true;

        os.str("");

        if(access(dnsBackupFile.c_str(), F_OK) == 0)
        {
            dnsArray = CFArrayCreateMutable(NULL, 0, NULL);

            systemDNSDumpFile.open(dnsBackupFile);

            while(std::getline(systemDNSDumpFile, line))
                CFArrayAppendValue(dnsArray, CFStringCreateWithFormat(NULL, NULL, CFSTR("%s"), line.c_str()));

            systemDNSDumpFile.close();

            dnsStore = SCDynamicStoreCreate(kCFAllocatorSystemDefault, CFSTR("AirVPNDNSRestore"), NULL, NULL);

            dnsDictionary = CFDictionaryCreate(NULL, (const void **)(CFStringRef []){ CFSTR("ServerAddresses") }, (const void **)&dnsArray, 1, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);

            dnsList = SCDynamicStoreCopyKeyList(dnsStore, CFSTR("Setup:/Network/(Service/.+|Global)/DNS"));

            listItems = CFArrayGetCount(dnsList);

            if(listItems > 0)
            {
                ndx = 0;

                while(ndx < listItems)
                {
                    setValueSuccess &= SCDynamicStoreSetValue(dnsStore, (CFStringRef)CFArrayGetValueAtIndex(dnsList, ndx), dnsDictionary);

                    ndx++;
                }
            }

            if(setValueSuccess == true)
                os << "Successfully restored system DNS.";
            else
            {
                os << "ERROR: Error while restoring DNS settings.";

                success = false;
            }
        }
        else
        {
            os << "WARNING: Backup copy of resolv.conf not found. DNS settings do not need to be restored.";

            success = true;
        }

        os << std::endl;

        if(stdOutput == false)
            OPENVPN_LOG_STRING(os.str());
        else
            std::cout << os.str();
    }

#endif

    unlink(dnsBackupFile.c_str());

    os.str("");

    if(mode == RestoreNetworkMode::FULL || mode == RestoreNetworkMode::FIREWALL_ONLY)
    {
        if(networkLockMode == NetFilter::Mode::OFF || netFilter == nullptr)
            localNetFilter = new NetFilter(resourceDirectory, NetFilter::Mode::AUTO);
        else
            localNetFilter = netFilter;

        if(localNetFilter != nullptr)
        {
            if(localNetFilter->restore())
                os << "Network filter successfully restored";
            else
            {
                os << "WARNING: Backup copy of network filter not found. Network settings do not need to be restored.";

                success = true;
            }
            
            if(networkLockMode == NetFilter::Mode::OFF)
                delete localNetFilter;
        }
        else
        {
            os << "ERROR: Cannot create NetFilter object.";

            success = false;
        }

        os << std::endl;

        if(stdOutput == false)
            OPENVPN_LOG_STRING(os.str());
        else
            std::cout << os.str();
    }

    return success;
}

void VpnClient::log(const ClientAPI::LogInfo &log)
{
    std::lock_guard<std::mutex> lock(log_mutex);

    std::cout << date_time() << ' ' << log.text << std::flush;
}

void VpnClient::clock_tick()
{
    const ClockTickAction action = clock_tick_action;
    clock_tick_action = CT_UNDEF;

    switch(action)
    {
        case CT_STOP:
        {
            std::cout << "signal: CT_STOP" << std::endl;

            stop();
        }
        break;

        case CT_RECONNECT:
        {
            std::cout << "signal: CT_RECONNECT" << std::endl;

            reconnect(0);
        }
        break;

        case CT_PAUSE:
        {
            std::cout << "signal: CT_PAUSE" << std::endl;

            pause("clock-tick pause");
        }
        break;

        case CT_RESUME:
        {
            std::cout << "signal: CT_RESUME" << std::endl;

            resume();
        }
        break;

        case CT_STATS:
        {
            std::cout << "signal: CT_STATS" << std::endl;

            print_stats();
        }
        break;

        default: break;
    }
}

void VpnClient::external_pki_cert_request(ClientAPI::ExternalPKICertRequest &certreq)
{
    if(!epki_cert.empty())
    {
        certreq.cert = epki_cert;
        certreq.supportingChain = epki_ca;
    }
    else
    {
        certreq.error = true;
        certreq.errorText = "external_pki_cert_request not implemented";
    }
}

void VpnClient::external_pki_sign_request(ClientAPI::ExternalPKISignRequest &signreq)
{
#if defined(USE_MBEDTLS)

    if(epki_ctx.defined())
    {
        try
        {
            // decode base64 sign request
            BufferAllocated signdata(256, BufferAllocated::GROW);
            base64->decode(signdata, signreq.data);

            // get MD alg
            const mbedtls_md_type_t md_alg = PKCS1::DigestPrefix::MbedTLSParse().alg_from_prefix(signdata);

            // log info
            OPENVPN_LOG("SIGN[" << PKCS1::DigestPrefix::MbedTLSParse::to_string(md_alg) << ',' << signdata.size() << "]: " << render_hex_generic(signdata));

            // allocate buffer for signature
            BufferAllocated sig(mbedtls_pk_get_len(epki_ctx.get()), BufferAllocated::ARRAY);

            // sign it
            size_t sig_size = 0;

            const int status = mbedtls_pk_sign(epki_ctx.get(), md_alg, signdata.c_data(), signdata.size(), sig.data(), &sig_size, rng_callback, this);

            if(status != 0)
                throw Exception("mbedtls_pk_sign failed, err=" + openvpn::to_string(status));

            if(sig.size() != sig_size)
                throw Exception("unexpected signature size");

            // encode base64 signature

            signreq.sig = base64->encode(sig);

            OPENVPN_LOG("SIGNATURE[" << sig_size << "]: " << signreq.sig);
        }
        catch(const std::exception& e)
        {
            signreq.error = true;
            signreq.errorText = std::string("external_pki_sign_request: ") + e.what();
        }
    }
    else

#endif

    {
        signreq.error = true;
        signreq.errorText = "external_pki_sign_request not implemented";
    }
}

int VpnClient::rng_callback(void *arg, unsigned char *data, size_t len)
{
    VpnClient *self = (VpnClient *)arg;

    if(!self->rng)
    {
        self->rng.reset(new SSLLib::RandomAPI(false));
        self->rng->assert_crypto();
    }

    return self->rng->rand_bytes_noexcept(data, len) ? 0 : -1; // using -1 as a general-purpose mbed TLS error code
}

bool VpnClient::pause_on_connection_timeout()
{
    return false;
}

#ifdef OPENVPN_REMOTE_OVERRIDE

bool VpnClient::remote_override_enabled()
{
    return !remote_override_cmd.empty();
}

void VpnClient::remote_override(ClientAPI::RemoteOverride &ro)
{
    RedirectPipe::InOut pio;
    Argv argv;
    argv.emplace_back(remote_override_cmd);
    OPENVPN_LOG(argv.to_string());

    const int status = system_cmd(remote_override_cmd, argv, nullptr, pio, RedirectPipe::IGNORE_ERR);

    if(!status)
    {
        const std::string out = string::first_line(pio.out);

        OPENVPN_LOG("REMOTE OVERRIDE: " << out);

        auto svec = string::split(out, ',');

        if(svec.size() == 4)
        {
            ro.host = svec[0];
            ro.ip = svec[1];
            ro.port = svec[2];
            ro.proto = svec[3];
        }
        else
            ro.error = "cannot parse remote-override, expecting host,ip,port,proto (at least one or both of host and ip must be defined)";
    }
    else
        ro.error = "status=" + std::to_string(status);
}

#endif

void VpnClient::raiseEvent(ClientEvent::Type event, const EventData &data)
{
    for(eventSubscriptionIterator = eventSubscription.begin(); eventSubscriptionIterator != eventSubscription.end(); eventSubscriptionIterator++)
    {
        if(eventSubscriptionIterator->first == event)
            (*eventSubscriptionIterator->second)(data);
    }
}

bool VpnClient::subscribeEvent(ClientEvent::Type event, EventCallbackFunction callbackFunction)
{
    bool subscriptionFound = false;

    if(callbackFunction == nullptr)
        return false;

    for(eventSubscriptionIterator = eventSubscription.begin(); eventSubscriptionIterator != eventSubscription.end(); eventSubscriptionIterator++)
    {
        if(eventSubscriptionIterator->second == callbackFunction)
            subscriptionFound = true;
    }

    if(subscriptionFound == true)
        return false;

    eventSubscription.insert(std::make_pair(event, callbackFunction));

    return true;
}

bool VpnClient::unsubscribeEvent(ClientEvent::Type event, EventCallbackFunction callbackFunction)
{
    bool subscriptionFound = false;

    if(callbackFunction == nullptr)
        return false;

    for(eventSubscriptionIterator = eventSubscription.begin(); eventSubscriptionIterator != eventSubscription.end(); eventSubscriptionIterator++)
    {
        if(eventSubscriptionIterator->second == callbackFunction)
        {
            subscriptionFound = true;
            
            eventSubscription.erase(eventSubscriptionIterator);
        }
    }

    return subscriptionFound;
}

std::vector<std::string> VpnClient::supportedDataCipher = { "AES-128-GCM", "AES-192-GCM", "AES-256-GCM", "CHACHA20-POLY1305" };

bool VpnClient::isDataCipherSupported(std::string cipher)
{
    return (std::find(supportedDataCipher.begin(), supportedDataCipher.end(), cipher) != supportedDataCipher.end());
}

std::vector<std::string> VpnClient::getSupportedDataCiphers()
{
    return std::vector<std::string>(supportedDataCipher);
}

#endif
